<?php
/**
 * aasd functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package aasd
 */


require( 'inc/aasd-theme-options.php' );
require( 'inc/aasd-shortcodes.php' );

require( 'inc/aasd-cpt-staff.php' );
require( 'inc/aasd-cpt-newsletter.php' );

require( 'inc/aasd-mb-staff.php' );
require( 'inc/aasd-mb-pages.php' );
require( 'inc/aasd-mb-newsletter.php' );

require( 'inc/aasd-tax-staff.php' );

require get_template_directory() . '/inc/template-tags.php';

add_action( 'init', array( 'AASD_Shortcodes', 'singleton' ) );

add_action( 'init', array( 'AASD_CPT_Staff', 'singleton' ) );
add_action( 'init', array( 'AASD_CPT_Newsletter', 'singleton' ) );

add_action( 'init', array( 'AASD_Metaboxes_Staff', 'singleton' ) );
add_action( 'init', array( 'AASD_Metaboxes_Page', 'singleton' ) );
add_action( 'init', array( 'AASD_Metaboxes_Newsletter', 'singleton' ) );

add_action( 'init', array( 'AASD_Taxonomy_Staff', 'singleton' ) );


if ( ! function_exists( 'aasd_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function aasd_setup() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'aasd' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		/*
		 * Registering new image sizes.
		*/
		add_image_size( 'gallery', 800, 600, false );
		add_image_size( 'hero', 1170, 400, true );
		add_image_size( 'staff', 180, 180, true );

	}
endif;
add_action( 'after_setup_theme', 'aasd_setup' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function aasd_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aasd' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'aasd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'aasd_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function aasd_scripts() {
	wp_enqueue_style( 'aasd-style', get_template_directory_uri() . '/style.min.css', array(), filemtime( get_template_directory() . '/style.min.css' ) );

	// footer scripts
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '//code.jquery.com/jquery-1.11.2.min.js', array(), false, false );
	wp_enqueue_script( 'jquery' );
	wp_deregister_script( 'jquery-ui-core' );
	wp_register_script( 'jquery-ui-core', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'jquery-ui-core' );

	wp_enqueue_script( 'aasd-main', get_template_directory_uri() . '/js/aasd-scripts.min.js', array(), filemtime( get_template_directory() . '/js/aasd-scripts.min.js' ), true );

}
add_action( 'wp_enqueue_scripts', 'aasd_scripts' );


// remove nasty features of WP
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/**
 * Custom walker class.
 */
class aasd_Walker_Nav_Menu extends Walker_Nav_Menu {

	/**
	 * Starts the list before the elements are added.
	 *
	 * Adds classes to the unordered list sub-menus.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// Depth-dependent classes.
		$indent			= ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$display_depth	= ( $depth + 1 ); // because it counts the first submenu as 0

		$classes = array(
			'dropdown-menu',
		);
		$class_names = implode( ' ', $classes );

		// $class_names	= join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
		// $class_names	= $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


		// Build HTML for output.
		$output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
	}

	/**
	 * Start the element output.
	 *
	 * Adds main/sub-classes to the list items and links.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent	= ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
		$caret	= '';

		if( $args->walker->has_children == 1 ) {
			$classes[]		= 'dropdown';
			$has_children	= 1;
		} else {
			// $classes[]	= '';
			$has_children	= 0;
		}

		$classes	= empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[]	= 'menu-item-' . $item->ID;

		$class_names	= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names	= $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


		// Build HTML.
		$output .= $indent . '<li' . $class_names . '>';

		// Link attributes.
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		if( $has_children == 1 ) {
			$attributes .= ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"';
			$caret = ' <span class="caret"></span>';
		}

		// Build HTML output and pass through the proper filter.
		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$caret,
			$args->link_after,
			$args->after
		);
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}


/**
 * Filters the content to remove any extra paragraph or break tags
 * caused by shortcodes.
 *
 * @since 1.0.0
 *
 * @param string $content  String of HTML content.
 * @return string $content Amended string of HTML content.
 */
function shortcode_empty_paragraph_fix( $content ) {
	$array = array(
		'<p>['		=> '[',
		']</p>'		=> ']',
		']<br />'	=> ']'
	);
	return strtr( $content, $array );

}
add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );


function aasd_custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'aasd_custom_excerpt_length', 999 );


/**
 * show staff member
 * repeating feature on site that displays a staff member
 *
 * @param  $id  post id
 */
function aasd_show_staff_member( $id ) {

	$prefix			= '_aasd_';
	$job_title		= apply_filters( 'the_content', get_post_meta( $id, $prefix . 'job_title', 1 ) );
	$filter_array	= array(
		'<p>'	=> '',
		'</p>'	=> '',
	);
	$job_filtered	= strtr( $job_title, $filter_array );
	$phone			= get_post_meta( $id, $prefix . 'phone', 1 );

	echo '<div class="col-special-3">
		<figure class="board-image"><img src="'. get_the_post_thumbnail_url( $id, 'staff' ) .'"></figure>
		<p><strong>'. get_the_title( $id ) .'</strong><br>'.
		$job_filtered .'<br>'.
		$phone .'</p>';

		if( is_user_logged_in() ) {
			echo '<p><a href="'. get_edit_post_link( $id ) .'" role="button">Edit</a></p>';
		}

	echo '</div>';

}


/**
 * gravity forms
 * prepopulate data
 */
add_filter( 'gform_field_value_staff_email', 'populate_staff_email' );
function populate_staff_email( $value ) {
	parse_str( $_SERVER['QUERY_STRING'], $queries );
	$id = $queries['id'] != '' ? $queries['id'] : '';

	$staff_email = ( get_post_meta( $id, '_aasd_email', 1 ) != '' ) ? get_post_meta( $id, '_aasd_email', 1 ) : aasd_get_option( '_aasd_fallback_email' );

	return $staff_email;
}