<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aasd
 */

?>
	<footer class="site-footer">
		<div class="container">
			<p>&copy; <?php echo date( 'Y' ); ?> &mdash; <?php echo get_bloginfo( 'name' ); ?></p>
		</div>
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
