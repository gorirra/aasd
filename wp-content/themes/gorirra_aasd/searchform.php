<form action="/" method="get" class="navbar-form">
	<div class="input-group">
		<input type="text" name="s" class="form-control" placeholder="Search for...">
		<span class="input-group-btn">
			<button class="btn btn-default" type="button">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</button>
		</span>
	</div>
</form>