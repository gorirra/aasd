<?php
	/**
	 * Template part for displaying page content in page.php
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package aasd
	 */

	$prefix				= '_aasd_';
	$newsletter_id		= get_the_ID();
	$newsletter_date	= date( 'M d, Y', get_post_meta( $newsletter_id, $prefix . 'newsletter_date', 1 ) );
	$newsletter_url		= get_post_meta( $newsletter_id, $prefix . 'newsletter_url', 1 );
?>


<div id="post-<?php the_ID(); ?>" <?php post_class( 'media' ); ?>>

	<div class="media-body">

		<header class="entry-header">

			<?php
				echo '<h4 class="media-heading"><a href="'. $newsletter_url .'" rel="bookmark">'. get_the_title() .'</a></h4>
				<p><small>'. $newsletter_date .'</small></p>';
			?>

		</header><!-- .entry-header -->

	</div>
</div>
