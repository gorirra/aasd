<?php
/**
 * Template part for displaying page content in staff.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */
$prefix	= '_aasd_';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php

			the_content();

			if( false === ( $officepersonnel = get_transient( 'officepersonnel_results' ) ) ) {

				$args = array(
					'post_type'			=> 'staff',
					'post_status'		=> 'publish',
					'posts_per_page'	=> -1,
					'order'				=> 'ASC',
					'orderby'			=> 'meta_value_num',
					'meta_key'			=> $prefix . 'order',
					'tax_query'			=> array(
						array(
							'taxonomy'	=> 'position_type',
							'field'		=> 'slug',
							'terms'		=> 'office-personnel'
						)
					),
				);

				$officepersonnel = new WP_Query( $args );

				set_transient( 'officepersonnel_results', $officepersonnel, 60 );

			}


			if( $officepersonnel->have_posts() ) {

				while( $officepersonnel->have_posts() ) {
					$officepersonnel->the_post();

					$id			= get_the_ID();
					$job_title	= get_post_meta( $id, $prefix . 'job_title', 1 );
					$email		= get_post_meta( $id, $prefix . 'email', 1 );

					echo '<div class="media staff-list">
						<div class="media-left">'.
							get_the_post_thumbnail( $id, 'staff', array( 'class' => 'media-object' ) ) .'
						</div>
						<div class="media-body">
							<h4 class="media-heading">'. get_the_title() .'</h4>
							<p><em>'. $job_title .'</em><br>
							Email: '. $email;

							if( is_user_logged_in() ) {
								echo '<br><a href="'. get_edit_post_link() .'" role="button">Edit</a>';
							}

							echo '</p>' .

							apply_filters( 'the_content', get_the_content() ) .'

						</div>
					</div>';

				}

				wp_reset_postdata();
			}

		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->


