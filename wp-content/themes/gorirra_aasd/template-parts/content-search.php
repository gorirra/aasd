<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

$is_event = 'tribe_events' === get_post_type() ? true : false;
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'media' ); ?>>

	<?php
		$thumbnail = get_the_post_thumbnail( get_the_ID() ) != '' ? '<div class="media-left"><a href="'. get_permalink() .'">'. get_the_post_thumbnail( get_the_ID(), 'thumbnail', array( 'class' => 'media-object' ) ) .'</a></div>' : '';

		echo $thumbnail;
	?>

	<div class="media-body">

		<header class="entry-header">

			<?php
				$event_label = $is_event == true ? 'Event: ' : '';

				echo '<h4 class="media-heading"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">'. $event_label . get_the_title() .'</a></h4>';

				if ( 'post' === get_post_type() ) : ?>

					<div class="entry-meta">
						<?php aasd_posted_on(); ?>
					</div><!-- .entry-meta -->

				<?php endif;
			?>

		</header><!-- .entry-header -->


		<?php if( !$is_event ) { ?>

			<div class="entry-summary">

				<?php the_excerpt(); ?>

			</div><!-- .entry-summary -->

		<?php } ?>

	</div>

</div>

