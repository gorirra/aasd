<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">

		<?php
			if ( is_singular() ) :
				the_title( '<h1 class="page-title">', '</h1>' );
			else :
				the_title( '<h2 class="page-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">', '</a></h2>' );
			endif;
		?>

	</header><!-- .page-header -->

	<div class="entry-content">
		<?php
			echo get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'alignleft' ) );

			the_content();

			wp_link_pages( array(
				'before'	=> '<div class="page-links">' . esc_html__( 'Pages:', 'aasd' ),
				'after'		=> '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
