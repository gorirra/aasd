<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">

		<?php
			if ( is_singular() ) :
				the_title( '<h1 class="page-title">', '</h1>' );
			else :
				the_title( '<h2 class="page-title"><a href="'. esc_url( get_permalink() ) .'" rel="bookmark">', '</a></h2>' );
			endif;
		?>

	</header><!-- .page-header -->

	<?php
		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php aasd_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php endif;
	?>

	<div class="entry-content">
		<?php
			the_excerpt();

			wp_link_pages( array(
				'before'	=> '<div class="page-links">' . esc_html__( 'Pages:', 'aasd' ),
				'after'		=> '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
