<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php
			echo '<h1 class="page-title">'. get_the_title() .'</h1>';

			$gallery_page_obj = get_page_by_path( 'photo-galleries' );

			if( $gallery_page_obj->ID === $post->post_parent ) {

				echo '<a href="'. get_permalink( end( get_ancestors( get_the_ID(), 'page' ) ) ).'">&laquo; Back to Galleries</a>';

			}
		?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'aasd' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
