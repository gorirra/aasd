<?php
/**
 * Template part for displaying page content in active-members.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */
$prefix	= '_aasd_';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php

			the_content();

			if( false === ( $activemembers = get_transient( 'activemembers_results' ) ) ) {

				$args = array(
					'post_type'			=> 'staff',
					'posts_per_page'	=> -1,
					'order'				=> 'ASC',
					'orderby'			=> 'name',
					'tax_query'			=> array(
						array(
							'taxonomy'	=> 'position_type',
							'field'		=> 'slug',
							'terms'		=> 'active-members'
						)
					),
				);

				$activemembers = new WP_Query( $args );

				set_transient( 'activemembers_results', $activemembers, 60 );

			}


			if( $activemembers->have_posts() ) {

				$count = 1;

				echo '<table class="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th><strong>Job Title</th>
							<th><strong>Work Location</th>';

							if( is_user_logged_in() ) {
								echo '<th>&nbsp;</th>';
							}

						echo '</tr>
					</thead>

					<tbody>';

						while( $activemembers->have_posts() ) {
							$activemembers->the_post();

							$id				= get_the_ID();
							$job_title		= get_post_meta( $id, $prefix . 'job_title', 1 );
							$work_location	= get_post_meta( $id, $prefix . 'work_location', 1 );

							echo '<tr>
								<td>'. get_the_title() .'</td>
								<td>'. $job_title .'</td>
								<td>'. $work_location .'</td>';

								if( is_user_logged_in() ) {
									echo '<td><a href="'. get_edit_post_link() .'">Edit</a></td>';
								}

							echo '</tr>';

							$count++;

						}

					echo '</tbody>
				</table>';

				wp_reset_postdata();
			}

		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->


