<?php
/**
 * Template part for displaying page content in staff.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */
$prefix	= '_aasd_';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php

			the_content();

			// default args
			$args = array(
				'post_type'			=> 'staff',
				'posts_per_page'	=> -1,
				'order'				=> 'ASC',
				'orderby'			=> 'meta_value_num',
				'meta_key'			=> $prefix . 'order',
				'tax_query'			=> array(
					array(
						'taxonomy'	=> 'position_type',
						'field'		=> 'slug',
						'terms'		=> 'board-of-directors'
					)
				),
			);


			/**
			 * officers
			 */
			if( false === ( $officers = get_transient( 'officers_results' ) ) ) {

				$officer_args = $args;

				$officer_args['meta_query'] = array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'officers',
						'compare'	=> 'LIKE',
					)
				);

				$officers = new WP_Query( $officer_args );

				set_transient( 'officers_results', $officers, 60 );

			}


			if( $officers->have_posts() ) {

				echo '<h2>Officers</h2>
				<div class="row">';

					while( $officers->have_posts() ) {
						$officers->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}


			/**
			 * directors
			 */
			if( false === ( $directors = get_transient( 'directors_results' ) ) ) {

				$director_args = $args;

				$director_args['orderby']		= 'meta_value';
				$director_args['meta_key']		= $prefix . 'last_name';
				$director_args['meta_query']	= array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'directors',
						'compare'	=> 'LIKE',
					)
				);

				$directors = new WP_Query( $director_args );

				set_transient( 'directors_results', $directors, 60 );

			}


			if( $directors->have_posts() ) {

				echo '<h2>Directors</h2>
				<div class="row">';

					while( $directors->have_posts() ) {
						$directors->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}


			/**
			 * representatives
			 */
			if( false === ( $representatives = get_transient( 'representatives_results' ) ) ) {

				$representative_args = $args;

				$representative_args['orderby']		= 'meta_value';
				$representative_args['meta_key']	= $prefix . 'last_name';
				$representative_args['meta_query']	= array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'representatives',
						'compare'	=> 'LIKE',
					)
				);

				$representatives = new WP_Query( $representative_args );

				set_transient( 'representatives_results', $representatives, 60 );

			}


			if( $representatives->have_posts() ) {

				echo '<h2>Representatives</h2>
				<div class="row">';

					while( $representatives->have_posts() ) {
						$representatives->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}


			/**
			 * ex-oficio
			 */
			if( false === ( $exoficio = get_transient( 'exoficio_results' ) ) ) {

				$exoficio_args = $args;

				$exoficio_args['meta_query'] = array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'exoficio',
						'compare'	=> 'LIKE',
					)
				);

				$exoficio = new WP_Query( $exoficio_args );

				set_transient( 'exoficio_results', $exoficio, 60 );

			}


			if( $exoficio->have_posts() ) {

				echo '<h2>Ex-Officio</h2>
				<div class="row">';

					while( $exoficio->have_posts() ) {
						$exoficio->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}

			/**
			 * office staff
			 */
			if( false === ( $officestaff = get_transient( 'officestaff_results' ) ) ) {

				$officestaff_args = $args;

				$officestaff_args['meta_query'] = array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'officestaff',
						'compare'	=> 'LIKE',
					)
				);

				$officestaff = new WP_Query( $officestaff_args );

				set_transient( 'officestaff_results', $officestaff, 60 );

			}


			if( $officestaff->have_posts() ) {

				echo '<h2>Office Staff</h2>
				<div class="row">';

					while( $officestaff->have_posts() ) {
						$officestaff->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}

		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->


