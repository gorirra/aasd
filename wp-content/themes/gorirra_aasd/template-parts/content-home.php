<?php
/**
 * Template part for displaying page content in front-page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

$prefix = '_aasd_';

	/**
	 * events
	 * get the next 3 events that will occur
	 */
	$today = date( 'Y-m-d' );

	if( false === ( $events_query = get_transient( 'events_query_results' ) ) ) {

		$events_args = array(
			'post_type'			=> 'tribe_events',
			'posts_per_page'	=> 3,
			'order'				=> 'ASC',
			'orderby'			=> 'meta_value',
			'meta_key' 			=> '_EventStartDate',
			'meta_query'		=> array(
				array(
					'key'		=> '_EventStartDate',
					'value'		=> $today,
					'compare'	=> '>=',
					'type'		=> 'DATE'
				)
			),
		);

		$events_query = new WP_Query( $events_args );

		set_transient( 'events_query_results', $events_query, 60 );

	}


	if( $events_query->have_posts() ) {

		echo '<h3>Events</h3>
		<div class="row">';

			while( $events_query->have_posts() ) {
				$events_query->the_post();

				$post_id = get_the_ID();

				$date_start_epoch	= strtotime( get_post_meta( $post_id, '_EventStartDate', 1 ) );
				$date_end_epoch		= strtotime( get_post_meta( $post_id, '_EventEndDate', 1 ) );

				$date_start	= date( 'Ymd', $date_start_epoch );
				$date_end	= date( 'Ymd', $date_end_epoch );

				$date_start_month	= date( 'm', $date_start_epoch );
				$date_end_month		= date( 'm', $date_end_epoch );

				/**
				 * date possibilities
				 */
				if( $date_start == $date_end ) {

					$date_display = date( 'M d, Y', $date_start_epoch );

				} elseif( $date_start_month != $date_end_month ) {

					$date_display = date( 'M d', $date_start_epoch ) .'&ndash;'. date( 'M d, Y', $date_end_epoch );

				} else {

					$date_display = date( 'M d', $date_start_epoch ) .'&ndash;'. date( 'd, Y', $date_end_epoch );

				}

				$time_start	= date( 'g:i a', $date_start_epoch );
				$time_end	= date( 'g:i a', $date_end_epoch );
				$all_day	= get_post_meta( $post_id, '_EventAllDay', 1 );

				if( $all_day != 'yes' ) {

					$date_display .= ' @ '. $time_start .'&ndash;'. $time_end;

				}

				echo '<div class="col-md-4">
					<h4>'. get_the_title() .'</h4>
					<p>'. $date_display .'</p>
					<p><a href="'. get_permalink() .'">More Info</a></p>
				</div>';

			}

		echo '</div>';

		if( $events_query->found_posts > 3 ) {

			echo '<p class="text-right"><a class="btn btn-default btn-sm" href="'. tribe_get_events_link() .'">See More Events &raquo;</a></p>';

		}

		wp_reset_postdata();

	} else {

		// no posts found

	}


	/**
	 * newsletters
	 * show the latest newsletter added (cpt)
	 */
	if( false === ( $newsletter_query = get_transient( 'newsletter_query_results' ) ) ) {

		$newsletter_args = array(
			'post_type'			=> 'newsletters',
			'post_status'		=> 'publish',
			'has_password'		=> false,
			'posts_per_page'	=> 1,
			'orderby'			=> 'meta_value_num',
			'meta_key'			=> $prefix . 'newsletter_date',
		);

		$newsletter_query	= new WP_Query( $newsletter_args );

		set_transient( 'newsletter_query_results', $newsletter_query, 60 );

	}


	if( $newsletter_query->have_posts() ) {
		while( $newsletter_query->have_posts() ) {
			$newsletter_query->the_post();

			$newsletter_id	= get_the_ID();
			$newsletter_url	= get_post_meta( $newsletter_id, $prefix . 'newsletter_url', 1 ) != '' ? get_post_meta( $newsletter_id, $prefix . 'newsletter_url', 1 ) : '#';

			$newsletter_item = '<div class="col-md-3">
				<figure><a href="'. $newsletter_url .'" target="_blank">'. get_the_post_thumbnail( $newsletter_id, 'medium', array( 'class' => 'img-responsive' ) ) .'</a></figure>
				<h4><a href="'. $newsletter_url .'" target="_blank">'. get_the_title() .'</a></h4>
				<p><small>'. date( 'M d, Y', get_post_meta( $newsletter_id, $prefix . 'newsletter_date', 1 ) ) .'</small></p>
				<p>' . get_the_excerpt() .'</p>
				<p><a href="'. $newsletter_url .'" target="_blank">Read More</a></p>
				<p class="text-right"><small><a class="btn btn-default btn-sm" href="'. site_url( '/newsletters/' ) .'">See More Newsletters &raquo;</a></small></p>
			</div>';

		}

		wp_reset_postdata();

	} else {

		$newsletter_item = '';

	}


	/**
	 * news
	 * get the next 3 or 4 news posts
	 * depending on whether a newsletter item exists
	 */
	$newsletter_posts_per_page = ( $newsletter_item != '' ) ? 3 : 4;

	if( false === ( $news_query = get_transient( 'news_query_results' ) ) ) {

		$news_args = array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'has_password'		=> false,
			'posts_per_page'	=> $newsletter_posts_per_page
		);

		$news_query	= new WP_Query( $news_args );

		set_transient( 'news_query_results', $news_query, 60 );

	}

	if( $news_query->have_posts() ) {

		echo '<h3>News</h3>
		<div class="row">'.

			$newsletter_item;

			while( $news_query->have_posts() ) {
				$news_query->the_post();

				echo '<div class="col-md-3">

					<figure><a href="'. get_permalink() .'"><img class="img-responsive" src="'. get_the_post_thumbnail_url( get_the_ID(), 'medium' ) .'"></a></figure>
					<h4><a href="'. get_permalink() .'">'. get_the_title() .'</a></h4>
					<p>' . get_the_excerpt() .'</p>
					<p><a href="'. get_permalink() .'">Read More</a></p>

				</div>';

			}

		echo '</div>';

		if( $news_query->found_posts > $newsletter_posts_per_page ) {

			echo '<p class="text-right"><a class="btn btn-default btn-sm" href="'. site_url( '/news-archives/' ) .'">See More News &raquo;</a></p>';

		}

		wp_reset_postdata();

	} else {

		// no news

	}

?>

