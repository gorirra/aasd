<?php
	/**
	 * Template part for displaying posts
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package aasd
	 */

	parse_str( $_SERVER['QUERY_STRING'], $queries );
	$id = $queries['id'] != '' ? $queries['id'] : '';

	if( $id != '' ) { ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="page-header">

				<?php
					the_title( '<h1 class="page-title">', ' '. get_the_title( $id ) .'</h1>' );
				?>

			</header><!-- .page-header -->

			<div class="entry-content">
				<?php
					$form_code = aasd_get_option( '_aasd_contact_form_shortcode' ) != '' ? apply_filters( 'the_content', aasd_get_option( '_aasd_contact_form_shortcode' ) ) : '';

					if( $form_code != '' ) {

						echo $form_code;

					}
				?>
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->

	<?php } else { ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<header class="page-header">
				<?php
					echo '<h1 class="page-title">'. get_the_title() .' Us</h1>';
				?>
			</header><!-- .page-header -->

			<div class="entry-content">
				<?php
					the_content();
				?>
			</div><!-- .entry-content -->


		</article><!-- #post-<?php the_ID(); ?> -->

	<?php }
?>

