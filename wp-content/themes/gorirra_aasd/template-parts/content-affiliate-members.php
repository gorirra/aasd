<?php
/**
 * Template part for displaying page content in active-members.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */
$prefix	= '_aasd_';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php

			the_content();

			if( false === ( $affiliatemembers = get_transient( 'affiliatemembers_results' ) ) ) {

				$args = array(
					'post_type'			=> 'staff',
					'posts_per_page'	=> -1,
					'order'				=> 'ASC',
					'orderby'			=> 'name',
					'tax_query'			=> array(
						array(
							'taxonomy'	=> 'position_type',
							'field'		=> 'slug',
							'terms'		=> 'affiliate-members'
						)
					),
				);

				$affiliatemembers = new WP_Query( $args );

				set_transient( 'affiliatemembers_results', $affiliatemembers, 60 );

			}


			if( $affiliatemembers->have_posts() ) {

				$found		= $affiliatemembers->found_posts;
				$foundhalf	= ceil( $found / 2 ) == 1 ? 2 : ceil( $found / 2 );
				$count		= 1;

				echo '<div class="row">';

					while( $affiliatemembers->have_posts() ) {
						$affiliatemembers->the_post();

						$id				= get_the_ID();
						$job_title		= get_post_meta( $id, $prefix . 'job_title', 1 );
						$work_location	= get_post_meta( $id, $prefix . 'work_location', 1 );

						if( $count == 1 ) {
							echo '<div class="col-sm-6">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Name</th>';

											if( is_user_logged_in() ) {
												echo '<th>Edit (Logged in only)</th>';
											}

										echo '</tr>
									</thead>

									<tbody>';
						}

							echo '<tr>
								<td>'. get_the_title() .'</td>';

								if( is_user_logged_in() ) {
									echo '<td><a href="'. get_edit_post_link() .'">Edit</a></td>';
								}

							echo '</tr>';

						if( $count == $foundhalf ) {
									echo '</tbody>
								</table>
							</div>
							<div class="col-sm-6">
								<table class="table table-striped">

									<thead>
										<tr>
											<th>Name</th>';

											if( is_user_logged_in() ) {
												echo '<th>Edit (Logged in only)</th>';
											}

										echo '</tr>
									</thead>

									<tbody>';
						} elseif( $count == $found ) {

									echo '</tbody>
								</table>
							</div>';

						}

						$count++;

					}

				echo '</div>';

				wp_reset_postdata();
			}

		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->


