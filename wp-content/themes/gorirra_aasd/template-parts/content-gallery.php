<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php
			if( false === ( $gallery_query = get_transient( 'gallery_query_results' ) ) ) {

				$args = array(
					'posts_per_page'	=> -1,
					'post_status'		=> 'publish',
					'has_password'		=> false,
					'post_type'			=> 'page',
					'post_parent'		=> get_the_ID()
				);

				$gallery_query = new WP_Query( $args );

				set_transient( 'gallery_query_results', $gallery_query, 60 );

			}


			if( $gallery_query->have_posts() ) {

				echo '<div class="row">';

					while( $gallery_query->have_posts() ) {
						$gallery_query->the_post();

						echo '<div class="col-special-3">

							<figure>
								<a href="'. get_permalink() .'"><img class="img-responsive" src="'. get_the_post_thumbnail_url( get_the_ID(), 'medium' ) .'"></a>
							</figure>
							<p><a href="'. get_permalink() .'">'. get_the_title() .'</a></p>

						</div>';

					}

				echo '</div>';

				wp_reset_postdata();
			} else {

				// no gallery subchildren
				echo '<p>There are no galleries at the moment.</p>';

			}

		?>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
