<?php
/**
 * Template part for displaying page content in staff.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */
$prefix	= '_aasd_';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .page-header -->

	<div class="entry-content">
		<?php

			the_content();

			// default args
			$args = array(
				'post_type'			=> 'staff',
				'posts_per_page'	=> -1,
				'order'				=> 'ASC',
				'orderby'			=> 'meta_value_num',
				'meta_key'			=> $prefix . 'order',
				'tax_query'			=> array(
					array(
						'taxonomy'	=> 'position_type',
						'field'		=> 'slug',
						'terms'		=> 'executive-board'
					)
				),
			);


			/**
			 * officers
			 */
			if( false === ( $executive_board_officers = get_transient( 'executive_board_officers_results' ) ) ) {

				$officer_args = $args;

				$officer_args['meta_query'] = array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'officers',
						'compare'	=> 'LIKE',
					)
				);

				$executive_board_officers = new WP_Query( $officer_args );

				set_transient( 'executive_board_officers_results', $executive_board_officers, 60 );

			}

			if( $executive_board_officers->have_posts() ) {

				echo '<h2>Officers</h2>
				<div class="row">';

					while( $executive_board_officers->have_posts() ) {
						$executive_board_officers->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}


			/**
			 * members
			 */
			if( false === ( $executive_board_members = get_transient( 'executive_board_members_results' ) ) ) {

				$member_args = $args;

				$member_args['orderby']		= 'meta_value';
				$member_args['meta_key']	= $prefix . 'last_name';
				$member_args['meta_query']	= array(
					array(
						'key'	=> $prefix . 'exec_board_member',
						'value'	=> 'on',
					)
				);

				$executive_board_members = new WP_Query( $member_args );

				set_transient( 'executive_board_members_results', $executive_board_members, 60 );

			}

			if( $executive_board_members->have_posts() ) {

				echo '<h2>Members</h2>
				<div class="row">';

					while( $executive_board_members->have_posts() ) {
						$executive_board_members->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}


			/**
			 * ex officio
			 */
			if( false === ( $executive_board_exofficio = get_transient( 'executive_board_exofficio_results' ) ) ) {

				$member_args = $args;

				$member_args['meta_query'] = array(
					array(
						'key'		=> $prefix . 'director_type',
						'value'		=> 'exoficio',
						'compare'	=> 'LIKE',
					)
				);

				$executive_board_exofficio = new WP_Query( $member_args );

				set_transient( 'executive_board_exofficio_results', $executive_board_exofficio, 60 );

			}

			if( $executive_board_exofficio->have_posts() ) {

				echo '<h2>Ex-Officio</h2>
				<div class="row">';

					while( $executive_board_exofficio->have_posts() ) {
						$executive_board_exofficio->the_post();

						aasd_show_staff_member( get_the_ID() );

					}

				echo '</div>';

				wp_reset_postdata();
			}

		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->


