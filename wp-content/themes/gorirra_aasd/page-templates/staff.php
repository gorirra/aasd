<?php
/**
 * Template Name: Staff
 */

get_header(); ?>

	<div class="container">

		<main id="main" class="site-main">

			<?php
				while ( have_posts() ) : the_post();

					$page_type = get_post_meta( get_the_ID(), '_aasd_staff_page_type', 1 );

					if( $page_type == 'retired' ) {

						get_template_part( 'template-parts/content', 'retired-members' );

					} elseif( $page_type == 'active' ) {

						get_template_part( 'template-parts/content', 'active-members' );

					} elseif( $page_type == 'affiliate' ) {

						get_template_part( 'template-parts/content', 'affiliate-members' );

					} elseif( $page_type == 'office' ) {

						get_template_part( 'template-parts/content', 'office-personnel' );

					} elseif( $page_type == 'board' ) {

						get_template_part( 'template-parts/content', 'board' );

					} elseif( $page_type == 'executive-board' ) {

						get_template_part( 'template-parts/content', 'executive-board' );

					} else {

						get_template_part( 'template-parts/content', 'none' );

					}


				endwhile; // End of the loop.
			?>

		</main><!-- #main -->

	</div>

<?php get_footer();
