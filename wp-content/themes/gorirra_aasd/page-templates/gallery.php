<?php
/**
 * Template Name: Photo Gallery Landing Page
 */

get_header(); ?>

	<div class="container">

		<main id="main" class="site-main">

			<?php
				get_template_part( 'template-parts/content', 'gallery' );
			?>

		</main><!-- #main -->

	</div>

<?php get_footer();
