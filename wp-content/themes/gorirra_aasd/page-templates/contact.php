<?php
/**
 * Template Name: Contact
 */

get_header(); ?>

	<div class="container">

		<main id="main" class="site-main">

			<?php while ( have_posts() ) {

				the_post();

				get_template_part( 'template-parts/content', 'contact' );

			}?>

		</main><!-- #main -->

	</div>

<?php get_footer();
