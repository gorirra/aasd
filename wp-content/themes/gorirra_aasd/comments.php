<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aasd
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
} ?>

<div id="comments" class="comments-area">

	<?php
		if ( have_comments() ) : ?>

			<h2 class="comments-title">

				<?php
					$comment_count = get_comments_number();

					if ( 1 === $comment_count ) {
						printf(
							/* translators: 1: title. */
							esc_html_e( 'One thought on &ldquo;%1$s&rdquo;', 'aasd' ),
							'<span>' . get_the_title() . '</span>'
						);
					} else {
						printf( // WPCS: XSS OK.
							/* translators: 1: comment count number, 2: title. */
							esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $comment_count, 'comments title', 'aasd' ) ),
							number_format_i18n( $comment_count ),
							'<span>' . get_the_title() . '</span>'
						);
					}
				?>
			</h2><!-- .comments-title -->

			<?php the_comments_navigation(); ?>

			<ol class="comment-list">
				<?php
					wp_list_comments( array(
						'style'			=> 'ol',
						'short_ping'	=> true,
					) );
				?>
			</ol><!-- .comment-list -->

			<?php the_comments_navigation();

			// If comments are closed and there are comments, let's leave a little note, shall we?
			if ( ! comments_open() ) : ?>
				<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'aasd' ); ?></p>
			<?php endif;

		endif; // Check for have_comments().


		/**
		 * modify comment field to work with bootstrap
		 */
		$commenter	= wp_get_current_commenter();
		$req		= get_option( 'require_name_email' );
		$aria_req	= ( $req ? " aria-required='true'" : '' );

		$fields		=  array(
			'author' => '<div class="form-group comment-form-author">
				<label for="author">' . __( 'Name' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label>
				<input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />
			</div>',
			'email' => '<div class="form-group comment-form-email">
				<label for="email">' . __( 'Email' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label>
				<input class="form-control" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />
			</div>',
		);

		$args = array(
			'comment_field' => '<div class="form-group comment-form-comment">
				<label for="comment">' . _x( 'Comment', 'noun' ) . '</label>
				<textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
			</div>',
			'class_submit' => 'btn btn-default',
			'fields' => apply_filters( 'comment_form_default_fields', $fields ),
		);

		comment_form( $args );
	?>

</div><!-- #comments -->
