<?php
/**
 * Custom Post Types | Staff Member
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */
class AASD_CPT_Staff {

	static $instance	= false;

	public function __construct() {

		$this->aasd_cpt_staff();

		if( isset( $_GET ) && !empty( $_GET['post_type'] ) ) {
			if( $_GET['post_type'] == 'staff' ) {
				$this->default_orderby_cpt_staff();
			}
		}


	}


	protected function aasd_cpt_staff() {
		$labels = array(
			'name'               => _x( 'Staff Member', 'post type general name' ),
			'singular_name'      => _x( 'Staff Member', 'post type singular name' ),
			'add_new'            => _x( 'Add New', 'Staff Member' ),
			'add_new_item'       => __( 'Add Staff Member' ),
			'edit_item'          => __( 'Edit Staff Member' ),
			'new_item'           => __( 'New Staff Member' ),
			'all_items'          => __( 'All Staff Members' ),
			'view_item'          => __( 'View Staff Member' ),
			'search_items'       => __( 'Search Staff Members' ),
			'not_found'          => __( 'No Staff Members Found' ),
			'not_found_in_trash' => __( 'No Staff Members Found in the Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => _x( 'Staff Members', 'admin menu' )
		);
		$args = array(
			'labels'			=> $labels,
			'description'		=> 'Staff Members',
			'public'			=> true,
			'menu_position'		=> 25,
			'supports'			=> array( 'author', 'title', 'thumbnail', 'category', 'editor' ),
			'has_archive'		=> true,
			'menu_icon'			=> 'dashicons-admin-users',
			'show_in_nav_menus'	=> true,
			'query_var'			=> true,
			'can_export'		=> true,
			// 'rewrite'			=> false,
			'capability_type'	=> 'post',
		);
		register_post_type( 'staff', $args );

	}


	protected function default_orderby_cpt_staff() {

		add_filter( 'manage_edit-staff_columns', 'aasd_extra_staff_columns' );
		function aasd_extra_staff_columns( $columns ) {
			$columns['pos_order'] =__( 'Order', 'aasd' );
			return $columns;
		}


		add_action( 'manage_staff_posts_custom_column', 'aasd_staff_column_content', 10, 2 );
		function aasd_staff_column_content( $column_name, $post_id ) {
			if( 'pos_order' != $column_name )
				return;
			//Get number of order from post meta
			$order = get_post_meta( $post_id, '_aasd_order', 1 );
			echo intval( $order );
		}


		add_filter( 'manage_edit-staff_sortable_columns', 'aasd_sortable_staff_column' );
		function aasd_sortable_staff_column( $columns ) {
			$columns['pos_order'] = 'pos_order';

			return $columns;
		}


		add_action( 'pre_get_posts', 'aasd_order_orderby' );
		function aasd_order_orderby( $query ) {
			if( !is_admin() )
				return;

			$orderby	= $query->get( 'orderby' );
			$order		= $query->get( 'order' );

			if( $orderby == '' || 'pos_order' == $orderby ) {
				$query->set( 'meta_key', '_aasd_order' );
				$query->set( 'orderby', 'meta_value_num' );

				if( $order == '' ) {

					$query->set( 'order', 'ASC' );

				}
			}
		}

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}

}
