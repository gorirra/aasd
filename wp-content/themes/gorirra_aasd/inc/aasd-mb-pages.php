<?php
/**
* Custom Metaboxes | Page
*/
class AASD_Metaboxes_Page {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function aasd_page_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_aasd_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'            => 'page-option',
			'title'         => __( 'Page Options', 'aasd' ),
			'object_types'  => array( 'page' ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_names'    => true, // Show field names on the left
			'show_on'		=> array( 'key' => 'page-template', 'value' => array(
				'page-templates/staff.php',
			) ),
		) );

		$cmb->add_field( array(
			'name'				=> 'Staff Page Type',
			'id'				=> $prefix . 'staff_page_type',
			'type'				=> 'select',
			'options_cb'		=> 'staff_page_types',
			'show_option_none'	=> true,
		) );

		function staff_page_types( $field ) {
			$types = array(
				'board'				=> 'Board of Directors',
				'executive-board'	=> 'Executive Board',
				'active'			=> 'Active Members',
				'affiliate'			=> 'Affiliate Members',
				'retired'			=> 'Retired Members',
				'office'			=> 'Office Personnel',
			);
			return $types;
		}
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'aasd_page_metaboxes' ) );
	}
}

