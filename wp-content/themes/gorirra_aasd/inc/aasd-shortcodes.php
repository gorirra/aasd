<?php
/**
 * AASD Shortcodes
 *
 * Adds custom shortcodes to the WordPress API.
 *
 */

class AASD_Shortcodes {

	public static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}


	/**
	 * WordPress Gallery Embed
	 *
	 * Overwrites the default WordPress gallery shortcode.
	 *
	 * Example:
	 * [gallery ids="" include="" exclude="" order="" orderby="" /]
	 *
	 * @link https://codex.wordpress.org/Gallery_Shortcode
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function gallery_embed( $attr = array(), $content = null ) {

		global $post;

		// check $post exists
		if ( empty( $post ) ) {
			return;
		}

		static $index = 0;
		$index++;

		if ( ! empty( $attr['ids'] ) ) {

			// 'ids' is explicitly ordered, unless you specify otherwise.
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}

			$attr['include'] = $attr['ids'];

		}

		$output = apply_filters( 'post_gallery', '', $attr, $content );

		if ( ! empty( $output ) ) {
			return $output;
		}

		$size = 'gallery';

		$html5 = current_theme_supports( 'html5', 'gallery' );

		$atts = shortcode_atts(
			array(
				'order'			=> 'ASC',
				'orderby'		=> 'menu_order ID',
				'id'			=> ( $post ? $post->ID : 0 ),
				'itemtag'		=> ( $html5 ? 'figure'     : 'dl' ),
				'icontag'		=> ( $html5 ? 'div'        : 'dt' ),
				'captiontag'	=> ( $html5 ? 'figcaption' : 'dd' ),
				'columns'		=> 1,
				'size'			=> $size,
				'include'		=> '',
				'exclude'		=> '',
				'link'			=> '',
				'autoplay'		=> ''
			),
			$attr,
			'gallery'
		);

		$id = intval( $atts['id'] );

		if ( ! empty( $atts['include'] ) ) {

			$attachments = array();

			$params = array(
				'post__in'			=> explode( ',', $atts['include'] ),
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				'order'				=> $atts['order'],
				'orderby'			=> $atts['orderby'],
				'posts_per_page'	=> -1
			);

			$_attachments = new WP_Query( $params );

			if ( $_attachments->have_posts() ) : while ( $_attachments->have_posts() ) : $_attachments->the_post();

				$attachments[$post->ID] = $post;

			endwhile; endif;

			wp_reset_postdata();

		} elseif ( ! empty( $atts['exclude'] ) ) {

			$params =
				array(
					'post_parent'		=> $id,
					'exclude'			=> $atts['exclude'],
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby']
				);

			$attachments = get_children( $params );

		} else {

			$params =
				array(
					'post_parent'		=> $id,
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby']
				);

			$attachments = get_children( $params );

		}

		if ( empty( $attachments ) ) {
			return '';
		}

		$valid_tags		= wp_kses_allowed_html( 'post' );

		$item_tag		= tag_escape( $atts['itemtag'] );
		$caption_tag	= tag_escape( $atts['captiontag'] );
		$icon_tag		= tag_escape( $atts['icontag'] );

		if ( ! isset( $valid_tags[ $item_tag ] ) ) {
			$item_tag = 'dl';
		}

		if ( ! isset( $valid_tags[ $caption_tag ] ) ) {
			$caption_tag = 'dd';
		}

		if ( ! isset( $valid_tags[ $icon_tag ] ) ) {
			$icon_tag = 'dt';
		}

		$columns		= intval( $atts['columns'] );
		$item_width		= ( $columns > 0 ? floor(100/$columns) : 100 );
		$float			= is_rtl() ? 'right' : 'left';

		$selector		= "gallery-{$index}";

		$gallery_style	= '';

		$size_class		= sanitize_html_class( $size );

		$gallery_div	= '<div class="owl-nav-custom">
			<div class="owl-prev"><span class="glyphicon glyphicon-menu-left"></span></div>
			<div class="owl-next"><span class="glyphicon glyphicon-menu-right"></span></div>
		</div>';

		$gallery_div .= sprintf('<div id="%s" class="gallery galleryid-%s owl-carousel owl-theme">', $selector, $id );

		$output			= apply_filters( 'gallery_style', $gallery_style . $gallery_div );

			foreach ( $attachments as $id => $attachment ) {
				$excerpt	= trim( $attachment->post_excerpt );

				$attr		= ( $excerpt ? array( 'aria-describedby' => "{$selector}-{$id}" ) : '' );

				// add fancybox support
				$image_output	= wp_get_attachment_link( $id, $size, false, false, false, $attr );
				$image_output	= str_replace( 'href', 'class="fancybox" data-fancybox="'. $selector .'" data-caption=\''. wptexturize($excerpt) .'\' href', $image_output );

				// set up lazy load on gallery images
				$image_output	= str_replace( 'attachment-gallery', 'attachment-gallery owl-lazy img-responsive', $image_output );
				$image_output	= str_replace( 'src', 'data-src', $image_output );

				// clean up the image caption

				if ( $caption_tag && ( ! empty( $attachment->post_excerpt ) || ! empty( $attachment->post_title ) ) ) {
					$caption_copy	= ( ! empty( $attachment->post_title ) ? "<h4>{$attachment->post_title}</h4>" : '' );
					$caption_copy	.= '<p>'. wptexturize($excerpt) .'</p>';

					$image_output .= sprintf(
						'<%1$s class="wp-caption-text gallery-caption" id="%2$s">%3$s</%1$s>',
						$caption_tag,
						$selector .'-'. $id,
						$caption_copy
					);

				}

				$output .= sprintf(
					'<%1$s class="gallery-item">%2$s</%1$s>',
					$item_tag,
					$image_output
				);

				if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
					$output .= '<br style="clear: both" />';
				}

			}

			if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
				$output .= "<br style='clear: both' />";
			}

		$output .= "</div>\n";

		return $output;
	}


	/**
	 * YouTube Embed
	 *
	 * Embeds the given YouTube video in the current post.
	 *
	 * Example:
	 * [youtube id="GjUXUN-F9Pg" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function youtube_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		return sprintf(
			'<div class="video-container">
				<iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>',
			$a['id']
		);

	}


	public function accordion_wrapper( $atts, $content = null ) {
		return '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			'. do_shortcode( $content ) .'
		</div>';
	}


	public function accordion_panel( $atts, $content = null ) {
		STATIC $i = 0;
		$i++;

		$a = shortcode_atts( array(
			'title' => '',
		), $atts );

		return '<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading'. $i .'">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'. $i .'" aria-expanded="true" aria-controls="collapse'. $i .'">'. $a['title'] .' <span class="glyphicon glyphicon-menu-down"></span></a>
				</h4>
			</div>

			<div id="collapse'. $i .'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'. $i .'">
				<div class="panel-body">'.
					do_shortcode( $content ) .'
				</div>
			</div>
		</div>';
	}


	public function googlemap_shortcode( $atts ) {
		$a = shortcode_atts( array(
			'url'   => ''
		), $atts );
		return '<iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$a['url'].'&output=embed"></iframe>
		<p><small><a href="'.$a['url'].'" target="_blank">View in a larger map</a></small></p>';
	}


	/**
	 * Bootstrap Row Embed
	 *
	 * Embeds a Bootstrap row in the current post.
	 *
	 * Example:
	 * [grid]Code is poetry. Silence is golden.[/grid]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function bootstrap_row_embed( $atts = array(), $content = null ) {

		$content = do_shortcode( trim( $content, '<br />' ) );
		$content = preg_replace( '/<\/div><br\ \/>/', '</div>', $content );

		return sprintf('<div class="row">%s</div>', $content );
	}

	/**
	 * Bootstrap Column Embed
	 *
	 * Embeds a Bootstrap column in the current post.
	 *
	 * Example:
	 * [grid]
	 *     [column count="2"]This is text wrapped in a 6-column div.[/column]
	 *     [column count="2"]This is text wrapped in a 6-column div.[/column]
	 * [grid]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function bootstrap_column_embed( $atts = array(), $content = null ) {

		$a = shortcode_atts(
			array(
				'count' => ''
			),
			$atts
		);

		$columns = intval( $a['count'] );

		// if count value isn't set, ignore shortcode
		if ( $columns < 1 || empty( $content ) ) {
			return $content;
		}

		switch ( $columns ) {
			case 2:
				$class = 'col-special-6';
				break;
			case 3:
				$class = 'col-special-4';
				break;
			case 4:
				$class = 'col-special-3';
				break;
			default:
				$class = 'col-special-6';
				break;
		}

		$content = trim( apply_filters( 'the_content', $content ) );

		return sprintf('<div class="%s">%s</div>', $class, $content );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self();

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		// override default gallery shortcode
		remove_shortcode( 'gallery' );
		add_shortcode( 'gallery',  array( $this, 'gallery_embed' ) );

		// register "youtube" shortcode
		add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );

		add_shortcode( 'accordion', array( $this, 'accordion_wrapper' ) );
		add_shortcode( 'panel', array( $this, 'accordion_panel' ) );

		add_shortcode( 'google-map', array( $this, 'googlemap_shortcode' ) );

		// register "grid" shortcode
		add_shortcode( 'grid', array( $this, 'bootstrap_row_embed' ) );
		add_shortcode( 'column', array( $this, 'bootstrap_column_embed' ) );

	}
}

