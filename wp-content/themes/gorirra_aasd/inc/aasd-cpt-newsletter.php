<?php
/**
 * Custom Post Types | Newsletter
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */
class AASD_CPT_Newsletter {

	static $instance	= false;

	public function __construct() {

		$this->aasd_cpt_newsletter();

	}


	protected function aasd_cpt_newsletter() {
		$labels = array(
			'name'               => _x( 'Newsletter', 'post type general name' ),
			'singular_name'      => _x( 'Newsletter', 'post type singular name' ),
			'add_new'            => _x( 'Add New', 'Newsletter' ),
			'add_new_item'       => __( 'Add Newsletter' ),
			'edit_item'          => __( 'Edit Newsletter' ),
			'new_item'           => __( 'New Newsletter' ),
			'all_items'          => __( 'All Newsletters' ),
			'view_item'          => __( 'View Newsletter' ),
			'search_items'       => __( 'Search Newsletters' ),
			'not_found'          => __( 'No Newsletters Found' ),
			'not_found_in_trash' => __( 'No Newsletters Found in the Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => _x( 'Newsletters', 'admin menu' )
		);
		$args = array(
			'labels'			=> $labels,
			'description'		=> 'Newsletters',
			'public'			=> true,
			'menu_position'		=> 25,
			'supports'			=> array( 'author', 'title', 'thumbnail', 'editor' ),
			'has_archive'		=> true,
			'menu_icon'			=> 'dashicons-media-text',
			'show_in_nav_menus'	=> true,
			'query_var'			=> true,
			'can_export'		=> true,
			// 'rewrite'			=> false,
			'capability_type'	=> 'post',
		);
		register_post_type( 'newsletters', $args );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}

}
