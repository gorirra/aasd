<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package aasd
 */

if ( ! function_exists( 'aasd_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function aasd_posted_on() {

		$time_string = '<span class="article__published">Published <strong><span><time class="entry-date published updated" datetime="%1$s">%2$s</time></span></strong></span>';

		if( get_the_time( 'Ymd' ) < get_the_modified_time( 'Ymd' ) ) {

			$time_string = '<span class="article__published">Published <strong><span><time class="entry-date published" datetime="%1$s">%2$s</time></span></strong></span>
			<span class="article__updated">Updated <strong><time class="updated" datetime="%3$s">%4$s</time></strong></span>';

		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);


		$posted_on = sprintf(
			esc_html_x( '%s', 'post date', 'velonews' ),
			$time_string
		);

		echo '<p class="article__byline">
			<small>'. $posted_on .'</small>
		</p>';
	}
endif;
