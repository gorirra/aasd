<?php
/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action( 'cmb2_admin_init', 'aasd_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function aasd_register_theme_options_metabox() {

	$prefix = '_aasd_';

	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'			=> $prefix . 'theme_options',
		'title'			=> esc_html__( 'AASD Options', 'aasd' ),
		'object_types'	=> array( 'options-page' ),

		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */

		'option_key'      => 'aasd_options', // The option key and admin menu page slug.
		// 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'aasd' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'aasd' ), // The text for the options-page save button. Defaults to 'Save'.
	) );

	/*
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */

	$cmb_options->add_field( array(
		'name'    => 'Important News - Banner',
		'desc'    => 'Enter some short text that will appear at the top of the site.',
		'id'      => $prefix . 'news_banner',
		'type'    => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 5,
		),
	) );

	$cmb_options->add_field( array(
		'name'    => 'Branding',
		'id'      => $prefix . 'branding_title',
		'type'    => 'title',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Site Logo',
		'desc'    => 'Upload an image or enter an URL.',
		'id'      => $prefix . 'logo',
		'type'    => 'file',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Site Brand Logo',
		'desc'    => 'Upload an image or enter an URL.',
		'id'      => $prefix . 'brandlogo',
		'type'    => 'file',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Business Name (Short)',
		'desc'    => 'Enter your shortened business name.',
		'id'      => $prefix . 'bizname_short',
		'type'    => 'text',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Business Name',
		'desc'    => 'Enter your business name.',
		'id'      => $prefix . 'bizname_long',
		'type'    => 'text',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Site Content',
		'id'      => $prefix . 'site_content_title',
		'type'    => 'title',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Homepage Hero Image',
		'desc'    => 'Dimensions: 1170x500 pixels (height can be anything). Upload an image or enter an URL.',
		'id'      => $prefix . 'hero',
		'type'    => 'file',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Contact Form',
		'desc'    => 'Enter the GravityForms contact form shortcode.',
		'id'      => $prefix . 'contact_form_shortcode',
		'type'    => 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name' => 'Fallback Email',
		'id'   => $prefix . 'fallback_email',
		'type' => 'text',
		'desc' => 'main point of contact for site. where default contact form will be sent to. separate multiple emails with a comma.'
	) );



}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function aasd_get_option( $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( 'aasd_options', $key, $default );
	}

	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( 'aasd_options', $default );

	$val = $default;

	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}

	return $val;
}
