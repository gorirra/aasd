<?php
class AASD_Taxonomy_Staff {

	static $instance = false;

	public function __construct() {
		$this->register_tax_staff();
	}

	// register two taxonomies to go with the post type
	protected function register_tax_staff() {
		// set up labels
		$labels = array(
			'name'              => 'Position Types',
			'singular_name'     => 'Position Type',
			'search_items'      => 'Search Position Types',
			'all_items'         => 'All Position Types',
			'edit_item'         => 'Edit Position Type',
			'update_item'       => 'Update Position Type',
			'add_new_item'      => 'Add New Position Type',
			'new_item_name'     => 'New Position Type',
			'menu_name'         => 'Position Types',
			'not_found'			=> 'No Position Types Found'
		);
		$args = array(
			'hierarchical'		=> true,
			'labels'			=> $labels,
			'query_var'			=> true,
			'show_admin_column'	=> true
		);
		// register taxonomy
		register_taxonomy( 'position_type', 'staff', $args );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

}
