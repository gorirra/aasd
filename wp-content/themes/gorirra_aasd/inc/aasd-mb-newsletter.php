<?php
/**
* Custom Metaboxes | Newsletter
*/
class AASD_Metaboxes_Newsletter {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function aasd_newsletter_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_aasd_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'newsletter',
			'title'			=> __( 'Newsletter Options', 'aasd' ),
			'object_types'	=> array( 'newsletters' ), // Post type
			'context'		=> 'normal',
			'priority'		=> 'high',
			'show_names'	=> true // Show field names on the left
		) );

		$cmb->add_field( array(
			'name'	=> 'Newsletter Date',
			'id'	=> $prefix . 'newsletter_date',
			'type'	=> 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name'	=> 'Newsletter URL',
			'id'	=> $prefix . 'newsletter_url',
			'type'	=> 'text_url',
			'desc'	=> 'enter the mailchimp url'
		) );


	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'aasd_newsletter_metaboxes' ) );
	}
}

