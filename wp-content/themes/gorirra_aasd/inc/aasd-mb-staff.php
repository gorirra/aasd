<?php
/**
* Custom Metaboxes | Staff
*/
class AASD_Metaboxes_Staff {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function aasd_faq_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_aasd_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'            => 'staff',
			'title'         => __( 'Staff Options', 'aasd' ),
			'object_types'  => array( 'staff' ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_names'    => true // Show field names on the left
		) );

		$cmb->add_field( array(
			'name'	=> 'First Name',
			'id'	=> $prefix . 'first_name',
			'desc'	=> 'used for sort feature',
			'type'	=> 'text',
		) );
		$cmb->add_field( array(
			'name'	=> 'Last Name',
			'id'	=> $prefix . 'last_name',
			'desc'	=> 'used for sort feature',
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Job Title',
			'id'      => $prefix . 'job_title',
			'type'    => 'textarea',
		) );

		$cmb->add_field( array(
			'name'    => 'Work Location',
			'id'      => $prefix . 'work_location',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Email',
			'id'      => $prefix . 'email',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Phone',
			'id'      => $prefix . 'phone',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'			=> 'Director Type',
			'id'			=> $prefix . 'director_type',
			'type'			=> 'multicheck',
			'desc'			=> 'choose a director type (Board of Directors page)',
			'options_cb'	=> 'director_types',
			'column'		=> true
		) );

		$cmb->add_field( array(
			'name'	=> 'Executive Board Member',
			'id'	=> $prefix . 'exec_board_member',
			'desc'	=> 'check ON if the person is an executive board member',
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'name'		=> 'Order',
			'id'		=> $prefix . 'order',
			'type'		=> 'text',
			'default'	=> 0,
			'desc'		=> 'For Board of Directors, Executive Board, and Office Personnel pages. Use multiples of 5.',
		) );

		function director_types( $field ) {
			$types = array(
				'officers'			=> 'Officer',
				'directors'			=> 'Director',
				'representatives'	=> 'Representative',
				'exoficio'			=> 'Ex-Officio',
				'officestaff'		=> 'Office Staff',
			);
			return $types;
		}
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'aasd_faq_metaboxes' ) );
	}
}

