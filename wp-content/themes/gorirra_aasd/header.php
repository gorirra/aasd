<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aasd
 */

?><!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>

	<?php
		$news_banner = aasd_get_option( '_aasd_news_banner' ) != '' ? aasd_get_option( '_aasd_news_banner' ) : '';

		if( $news_banner != '' ) {

			echo '<div class="newsbanner">
				<div class="container">'.

					$news_banner .'

				</div>
			</div>';
		}
	?>


	<header class="site-header">

		<div class="container">
			<div class="site-header__top">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo aasd_get_option( '_aasd_logo' ); ?>" title=""></a>

				<?php
					echo '<div class="navbar-search">' .
						get_search_form( false ) . '
					</div>';
				?>
			</div>
		</div>

		<nav class="navbar navbar-default">
			<div class="container">

				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<a href="<?php echo site_url(); ?>" class="navbar-brand">
						<img src="<?php echo aasd_get_option( '_aasd_brandlogo' ); ?>">
						<span class="navbar-brand-short"><?php echo aasd_get_option( '_aasd_bizname_short' ); ?></span>
						<span class="navbar-brand-long"><?php echo aasd_get_option( '_aasd_bizname_long' ); ?></span>
					</a>

				</div>

				<div id="navbar" class="collapse navbar-collapse">
					<?php
						$args = array(
							'menu'				=> 'primary',
							'menu_class'		=> 'nav navbar-nav',
							'container'			=> '',
							'fallback_cb'		=> false,
							'walker'			=> new aasd_Walker_Nav_Menu()
						);
						wp_nav_menu( $args );
					?>
					<div class="navbar-search">
						<?php echo get_search_form( false ); ?>
					</div>
				</div>
			</div>

		</nav>

	</header>


	<?php
		/**
		 * show jumbotron and subnav on homepage only
		 */
		if( is_front_page() ) { ?>

			<div class="jumbotron">
				<div class="container">
					<img class="jumbotron-image img-responsive" src="<?php echo aasd_get_option( '_aasd_hero' ); ?>" alt="">
				</div>
			</div>

		<?php }
	?>
