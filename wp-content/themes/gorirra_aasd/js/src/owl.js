$(document).ready(function() {

	var $owl = $( '.owl-carousel' );

	if( $owl.length > 0 ) {

		$owl.owlCarousel({
			items: 1,
			dots: true,
			lazyLoad: true,
			navContainer: 'owl-nav',
		});

		$( '.owl-prev' ).click( function() {
			$owl.trigger( 'prev.owl.carousel' );
		});
		$( '.owl-next' ).click( function() {
			$owl.trigger( 'next.owl.carousel' );
		});
	}
});