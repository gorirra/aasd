$( document ).ready( function() {

	var $panel = $( '.panel-group' );

	if( $panel.length > 0 ) {

		$( '.collapse' ).on( 'show.bs.collapse', function() {

			$( this ).parent().find( '.glyphicon-menu-down' ).removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-up' );

		}).on( 'hide.bs.collapse', function() {

			$( this ).parent().find( '.glyphicon-menu-up' ).removeClass( 'glyphicon-menu-up' ).addClass( 'glyphicon-menu-down' );

		});

	}

});