# AASD User Guide Plugin
This plugin adds documentation and helpful tips for users working with the AASD site. These are dev notes.

## Headings
`<h2 class="user-guide__heading">Heading 2</h2>`

`<h3 class="user-guide__section-heading">Heading 3</h3>`

## Bullets
`<ul class="user-guide__list">...</ul>`

## Links
`<a href="<?php echo admin_url('admin.php?page=QUERY') ?>" target="_blank">Link Text</a>`