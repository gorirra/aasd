<?php
	/**
	 * custom post types
	 */
?>
<h2 class="user-guide__heading">Custom Post Types</h2>

<h3 class="user-guide__section-heading">Team Members <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=team') ?>">Link</a></h3>

<p>Add/Modify AASD team members here. Will appear on the home and about pages. After clicking add (or editing one):</p>

<ul class="user-guide__list">
	<li><strong>Main Title:</strong> Team member's name.</li>
	<li><strong>Content Editor:</strong> Team member's bio.</li>
	<li><strong>Featured Image:</strong> Team member's photo (Dimensions: 400 x 400 px -- must be square!).</li>
	<li>
		<strong>Team Member Options:</strong> Enter the team member's title and various social media URL's. Leave it blank if you don't want it to appear on the site.

		<ul class="user-guide__list__sections">
			<li><strong>Order:</strong> Use multiples of 5 to order the team members (Using "5" just in case you need to add members between two existing ones -- just makes it easier).</li>
		</ul>
	</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Press <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=press') ?>">Link</a></h3>

<p>Add/Modify AASD press pieces here. Will appear on the press page. After clicking add (or editing one):</p>

<ul class="user-guide__list">
	<li><strong>Main Title:</strong> Article Title.</li>
	<li><strong>Content Editor:</strong> Article excerpt.</li>
	<li>
		<strong>Press Options:</strong> Enter the publication name (Press Name), date of publication, and the URL to the article.

		<ul class="user-guide__list__sections">
			<li><strong>Stock Image:</strong> Optional. You can upload an image here to supplement the press tile (Dimensions: 400 x 200 px). Make sure not to upload copyrighted material.</li>
		</ul>
	</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Testimonials <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=testimonials') ?>">Link</a></h3>

<p>Add/Modify AASD client testimonials here. Will appear on the home and events pages. After clicking add (or editing one):</p>

<ul class="user-guide__list">
	<li><strong>Main Title:</strong> Enter a unique (short) name to help it stand out if you need to edit (only shows in admin).</li>
	<li>
		<strong>Testimonial Options:</strong>

		<ul class="user-guide__list__sections">
			<li><strong>Logo:</strong> Enter the logo from the testimonial (Dimension: 100 x 100 px).</li>
			<li><strong>Testimonial:</strong> Enter the quote. Optional: You can use the <code>[button]</code> shortcode to add links to that person's website.</li>
		</ul>
	</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Events <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=event') ?>">Link</a></h3>

<p>Add/Modify AASD events here. Will appear on the events and solutions pages. After clicking add (or editing one):</p>

<ul class="user-guide__list">
	<li><strong>Main Title:</strong> City and state of the event.</li>
	<li><strong>Content Editor:</strong> Description of the event. Appears on the register landing page for the event.</li>
	<li>
		<strong>Single Event Options:</strong>

		<ul class="user-guide__list__sections">
			<li><strong>Start and End Date:</strong> Note: The site will only show events that happen in the future -- all old events will just not appear (to avoid confusion for registrants).</li>
			<li><strong>Location (Hotel):</strong> Enter the location where the event will be held.</li>
			<li><strong>Location Image:</strong> Upload an image of the host city (Dimensions: 300 x 300 px).</li>
		</ul>
	</li>
</ul>
