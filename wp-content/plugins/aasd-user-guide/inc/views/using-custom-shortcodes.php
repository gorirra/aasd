<?php
	/**
	 * shortcodes
	 */
?>
<h2 class="user-guide__heading">Using Custom Shortcodes</h2>

<h3 class="user-guide__section-heading">YouTube</h3>

<p>The youtube shortcode allows you to embed YouTube videos inside WordPress pages/posts.</p>

<table class="user-guide__table">
	<thead>
		<tr>
			<th>Option</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>id</td>
			<td>The id of the YouTube video</td>
		</tr>
	</tbody>
</table>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[youtube id="Q0CbN8sfihY"]</pre>

<p>***</p>

<h3 class="user-guide__section-heading">Google Map</h3>

<p>The google map shortcode allows you to embed a Google Map inside WordPress pages/posts.</p>

<table class="user-guide__table">
	<thead>
		<tr>
			<th>Option</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>url</td>
			<td>The url of the Google map</td>
		</tr>
	</tbody>
</table>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[google-map url="http://maps.google.com/maps...&output=embed"]</pre>

<p>***</p>

<h3 class="user-guide__section-heading">Grid and Columns</h3>

<p>The Grid and Columns shortcodes allow you to create grids inside your posts. While the Grid shortcode has no available options, the opening [grid] and closing [/grid] tags are required.

<table class="user-guide__table">
	<thead>
		<tr>
			<th>Option</th>
			<th>Description</th>
			<th>Example</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>count</td>
			<td>The number of columns you want to create (2, 3, or 4).</td>
			<td>[column count="2"]</td>
		</tr>
	</tbody>
</table>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>
[grid]
	[column count="2"]
		Lorem ipsum dolor sit amet, consectetur...
	[/column]
	[column count="2"]
		Ut ullamcorper ullamcorper elit et bibendum...
	[/column]
[/grid]
</pre>

<p>***</p>

<h3 class="user-guide__section-heading">Accordions and Panels</h3>

<p>The Accordion and Panels shortcodes allow you to create expandable/collapsible panels inside your post/page. While the accordion shortcode has no available options, the opening [accordion] and closing [/accordion] tags are required.</p>

<table class="user-guide__table">
	<thead>
		<tr>
			<th>Option</th>
			<th>Description</th>
			<th>Example</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>title</td>
			<td>Title for the panel.</td>
			<td>[panel title="Panel Number 3"]...[/panel]</td>
		</tr>
		<tr>
			<td>[content]</td>
			<td>Content that appears within the accordion panel.</td>
			<td>[panel title="Panel Number 3"]Content goes here[/panel]</td>
		</tr>
	</tbody>
</table>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>
[accordion]
	[panel title="Title 1"]
		Lorem ipsum dolor sit amet, consectetur...
	[/panel]
	[panel title="Title 2"]
		Ut ullamcorper ullamcorper elit et bibendum...
	[/panel]
[/accordion]
</pre>

<p>***</p>