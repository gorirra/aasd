<?php
	/**
	 * pages/templates
	 */
?>
<h2 class="user-guide__heading">Pages/Templates</h2>

<p>Nearly every piece of content (text/images) can be modified in the admin (Please let Gorirra Consulting know if there's something that isn't quite modifiable). Simply edit the page you'd like to modify and scroll beneath the main text editor to see the various file uploads and text editors and input fields. If you are unsure about something, just ask us and we'll be glad to help!</p>

<p>All special pages are setup with their respective templates. There is no need to make modifications to this.</p>

<p>***</p>

<h3 class="user-guide__section-heading">Posts <a class="page-title-action" href="<?php echo admin_url('edit.php') ?>">Link</a></h3>

<p>To add a new blog post, click <a href="<?php echo admin_url('post-new.php') ?>">Add New</a>. The most recent posts will appear on the home page</p>

<ul class="user-guide__list">
	<li><strong>Title:</strong> Enter the name for the post</li>
	<li><strong>Featured Image:</strong> Upload an image for the post. Image will be resized and cropped down to 300x169 pixels.</li>
	<li><strong>Content:</strong> Enter the content for the post.</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Newsletter <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=newsletters') ?>">Link</a></h3>

<p>To add a newsletter, click <a href="<?php echo admin_url('post-new.php?post_type=newsletters') ?>">Add New</a>. The most recent newsletter will appear on the home page</p>

<ul class="user-guide__list">
	<li><strong>Title:</strong> Enter the title for the newsletter</li>
	<li><strong>Featured Image:</strong> Upload an image to appear on the homepage</li>
	<li><strong>Newsletter Date:</strong> Enter the date for the newsletter</li>
	<li><strong>Newsletter URL:</strong> Enter the Mailchimp URL for the newsletter</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Staff <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=staff') ?>">Link</a></h3>

<p>To add a staff member, click <a href="<?php echo admin_url('post-new.php?post_type=staff') ?>">Add New</a>.</p>

<ul class="user-guide__list">
	<li><strong>Title:</strong> Enter the name for the staff member</li>
	<li><strong>Featured Image:</strong> Upload the staff member's photo. Square is preferred as a 180x180 pixel image will be displayed.</li>
	<li><strong>Content:</strong> Enter the staff member's bio.</li>
	<li><strong>Staff Options:</strong> Fill in the rest of the information for the staff member. If the staff member has multiple job titles, simply put a title on each line. Certain member types are specially sorted: Board of Directors (Officers, Ex Officio), Executive Board (Officers, Ex Officio), and Office Personnel. It is suggested to try to use numbers in multiples of 5's -- this helps to squeeze new staff members in between two others.</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Events <a class="page-title-action" href="<?php echo admin_url('edit.php?post_type=tribe_events') ?>">Link</a></h3>

<p>To add an event, click <a href="<?php echo admin_url('post-new.php?post_type=tribe_events') ?>">Add New</a>.</p>

<ul class="user-guide__list">
	<li><strong>Title:</strong> Enter the title for the event</li>
	<li><strong>Content:</strong> Enter text for the detail page for the event</li>
	<li><strong>The Events Calendar:</strong> Fill in the fields</li>
</ul>

<p>***</p>

<p><em>* Gravity Forms should be created by Gorirra Consulting as there are many options to make it a) look proper and b) actually post to an email properly.</em></p>