<?php
	/**
	 * site options
	 */
?>
<h2 class="user-guide__heading">Site Options <a class="page-title-action" href="<?php echo admin_url('admin.php?page=aasd_options') ?>">Link</a></h2>

<p>Global site options are stored here.</p>

<ul class="user-guide__list">
	<li><strong>Important News - Banner:</strong> Text entered here will display at the top of the website as a banner.</li>
	<li><strong>Branding:</strong> Upload site logos, preferably in SVG format. Enter both the short and full business names.</li>
	<li><strong>Site Content:</strong> Upload the homepage hero image (Dimensions: 1170x500). Enter the Gravity Form shortcode to the Contact Form field.* Fallback email is the email that generic forms will be sent to.</li>
</ul>

<p>Note: Cache is set to 1 minute. Changes to any staff member will reflect on the live site in about a minute or so.</p>

<p><em>* Gravity Forms should be created by Gorirra Consulting as there are many options to make it a) look proper and b) actually post to an email properly. Please let us know if the endpoints ever need to be changed.</em></p>
