<div class="wrap user-guide">

	<h1>AASD User Guide</h1>

	<h2 class="nav-tab-wrapper">

		<a href="<?php echo admin_url('admin.php?page=aasd-user-guide') ?>" class="nav-tab <?php echo ($active_tab === 'table-of-contents' ? 'nav-tab-active' : '') ?> "><?php _e('Table of Contents', 'aasd'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=site-options') ?>" class="nav-tab <?php echo ($active_tab == 'site-options' ? 'nav-tab-active' : ''); ?>"><?php _e('Site Options', 'aasd'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=pages-templates') ?>" class="nav-tab <?php echo ($active_tab == 'pages-templates' ? 'nav-tab-active' : ''); ?>"><?php _e('Pages/Templates', 'aasd'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=using-custom-shortcodes') ?>" class="nav-tab <?php echo ($active_tab == 'using-custom-shortcodes' ? 'nav-tab-active' : ''); ?>"><?php _e('Using Custom Shortcodes', 'aasd'); ?></a>

	</h2>

	<div class="user-guide__content">
		<?php require(AASD_USER_GUIDE_DIR ."inc/views/{$active_tab}.php"); ?>
	</div>

</div>
