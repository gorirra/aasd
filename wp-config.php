<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zSA8adYuAoPZLKjEWzfYNidnyRUh1youMOhABu3h+AyCbLOLnqd2tX44AmPi/2gmlkXKvqDEZQCtJpJK5ClSOw==');
define('SECURE_AUTH_KEY',  'L1lrNiXQr6ogu86jWVsqOyfsngCpn1+ZP8NJ5r8F9bcZRUbAs0HlEHxwiB5dRr7RPWuEICRnI+d7qZxjAL8ROg==');
define('LOGGED_IN_KEY',    'dfgiCVIIyuLEnBWL00tKdzyb7eDAxYiD7BBtvrosp8ZO5o+kvm7jmwV6Px+mmVM7VQjz4pr47rsVXrKkBHLC9g==');
define('NONCE_KEY',        'Dfi+8xYZSO184vwnpCwD9xH54dkV8tfNz2W7OJwHQ78wS6vstlAMTolt5qh3uS44eM7spex94/Gg0vsInzscFw==');
define('AUTH_SALT',        'fL1RfiJFQRWR1RIoQmQpJ42nzpuirsOriWBaSwGC3G2txB+o97q+49s676png/UUo5UcIqyD70e9MvP9ca5jCQ==');
define('SECURE_AUTH_SALT', 'maoKw/3sI8hSNro/l9C10ts6An68UCgd/ApjDYw8pAaW1T30t9KN3ovi+tAnZqwHKhdd7zNncOP2FnvmTUktTg==');
define('LOGGED_IN_SALT',   'af+1EdlzHWLLNiOrShlfUQ6MARTISzPi5dWSo1inBdRSggx7WzLD0pvCyE2Zz8n85YpeW7KTMoWxLwU6RBI69w==');
define('NONCE_SALT',       '2ARc4Zm8bGF4O0L6pDM3dz04lQhwwJypQSkluWv5IOirGmCaXWKO6NFTVAVY6YYPQ7pHUZrROJHs4jLLegDbbg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
